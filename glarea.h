// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <algorithm>

#include <QKeyEvent>
#include <QTimer>
#include <QElapsedTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

#include "bresenham.h"
#include "mymatrix4x4.h"
#include "graduations.h"

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();

protected slots:
    void onTimeout();
    void changeFP_x(int x) { changeFirstPoint(QVector2D(x, points.at(0).y())); }
    void changeFP_y(int y) { changeFirstPoint(QVector2D(points.at(0).x(), y)); }
    void changeLP_x(int x) { changeLastPoint (QVector2D(x, points.at(points.size()-1).y())); }
    void changeLP_y(int y) { changeLastPoint (QVector2D(points.at(points.size()-1).x(), y)); }
    void changeIN3D(bool c){ in3D = c; rulers.changeDisplay(); resetPosition(); }
    void increase();
    void decrease();
    void changeAngleObj(double a);
    void tourner();
    void changerMode(int m);

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

private:
    enum modes { MODE_CARRE, MODE_CERCLE };

    float xRot=0.0f, yRot=0.0f, zRot=0.0f;
    float xPos=0.0f,  yPos=0.0f, zPos=-20.0f;
    float auto_rot=0.0f;
    float auto_angle = 90.0f;
    bool auto_rotate = false;
    QTimer *timer = nullptr;
    QElapsedTimer elapsedTimer;
    unsigned nb_tri;

    modes mode;

    std::vector<QVector2D> points;
    int nbDisplayed = 0;
    bool in3D = false;

    Graduations rulers;

    float dt = 0;
    float windowRatio = 1.0f;
    QPoint lastPos;

    QOpenGLShaderProgram *m_program;

    void makeGLObjects();
    void tearGLObjects();
    QOpenGLBuffer m_vbo;
    QOpenGLBuffer m_vbo_points;
    QOpenGLBuffer m_vbo_circles;
    QOpenGLBuffer m_vbo_points_circles;

    void changeFirstPoint(QVector2D p);
    void changeLastPoint (QVector2D p);
    void reloadPoints(Bresenham b);
    void resetPosition();

    QVector<GLfloat> generateSquareVertices();
    QVector<GLfloat> generateCircleVertices();

};

#endif // GLAREA_H
