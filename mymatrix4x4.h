#ifndef MYMATRIX4X4_H
#define MYMATRIX4X4_H

#include "mymatrix.h"
#include <QMatrix4x4>

class MyMatrix4x4 : MyMatrix
{
private:
public:
    MyMatrix4x4();

    void rotate(float angle, float x, float y, float z);
    void translate(float x, float y, float z);
    void scale(float x, float y, float z);

    void perspective(float verticalAngle, float aspectRatio, float nearPlane, float farPlane);
    void frustum(float left, float right, float bottom, float top, float nearPlane, float farPlane);

    void rotate(float angle, QVector3D v)   { rotate(angle,v.x(),v.y(),v.z()); }
    void translate(QVector3D v)             { translate(v.x(),v.y(),v.z()); }
    void scale(QVector3D v)                 { scale(v.x(),v.y(),v.z()); }

    QMatrix4x4 matrix();
    QMatrix4x4 matrixAtPoint(QVector3D p);
};

#endif // MYMATRIX4X4_H
