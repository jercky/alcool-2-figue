#ifndef GRADUATIONS_H
#define GRADUATIONS_H

#include "graduatedruler.h"
#include "grid.h"
#include "mymatrix4x4.h"

class Graduations
{
private:
    std::vector<GraduatedRuler> rulers;
    Grid grid;
    bool display3D = false;
public:
    Graduations();
    void display(QOpenGLShaderProgram* sp);
    void changeDisplay();
};

#endif // GRADUATIONS_H
