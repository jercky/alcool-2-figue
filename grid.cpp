#include "grid.h"

unsigned Grid::height_ = 50;
unsigned Grid::rate_   = 1;

Grid::Grid(QVector3D color)
    :color_(color)
{
    nb_line = 4*(height_);
    GLfloat* vertices = new GLfloat[nb_line*2*3];

//    vertices[0] = 0.; vertices[1] = -int(height_); vertices[2] = 0.;
//    vertices[3] = 0.; vertices[4] =  height_; vertices[5] = 0.;

    unsigned grad = 0;
    for (int i = -int(height_) ; i <= int(height_) ; i+=rate_)
    {
        if (i != 0)
        {
            vertices[grad]   = i;
            vertices[grad+1] = height_;
            vertices[grad+2] = 0.01f;

            vertices[grad+3] = i;
            vertices[grad+4] = -int(height_);
            vertices[grad+5] = 0.01f;

            vertices[grad+6] = height_;
            vertices[grad+7] = i;
            vertices[grad+8] = 0.01f;

            vertices[grad+9]  = -int(height_);
            vertices[grad+10] = i;
            vertices[grad+11] = 0.01f;

            grad += 12;
        }
    }

    QVector<GLfloat> vertData;
    for (int i = 0; i < nb_line*2; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData.append(vertices[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 3; j++)
            vertData.append(color[j]);
    }

    vbo_.create();
    vbo_.bind();
    vbo_.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
}

void Grid::display(QOpenGLShaderProgram *sp)
{
    vbo_.bind();

    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");
    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6*sizeof(GLfloat));

    glDrawArrays(GL_LINES, 0, nb_line*3);

    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");

    vbo_.release();
}
