// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "princ.h"
#include <QDebug>

Princ::Princ(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::Princ)
{
    ui->setupUi(this);
    connect(ui->dbox_x, SIGNAL(valueChanged(int)), ui->glarea, SLOT(changeFP_x(int)));
    connect(ui->dbox_y, SIGNAL(valueChanged(int)), ui->glarea, SLOT(changeFP_y(int)));
    connect(ui->abox_x, SIGNAL(valueChanged(int)), ui->glarea, SLOT(changeLP_x(int)));
    connect(ui->abox_y, SIGNAL(valueChanged(int)), ui->glarea, SLOT(changeLP_y(int)));
    connect(ui->proj3D, SIGNAL(clicked(bool))    , ui->glarea, SLOT(changeIN3D(bool)));
    connect(ui->augmenter, SIGNAL(pressed())     , ui->glarea, SLOT(increase()));
    connect(ui->decrementer, SIGNAL(pressed())   , ui->glarea, SLOT(decrease()));
    connect(ui->box_angle, SIGNAL(valueChanged(double)), ui->glarea, SLOT(changeAngleObj(double)));
    connect(ui->tourner,   SIGNAL(pressed()),            ui->glarea, SLOT(tourner()));
    connect(ui->box_mode, SIGNAL(currentIndexChanged(int)), ui->glarea, SLOT(changerMode(int)));
}
