#include "mymatrix4x4.h"

MyMatrix4x4::MyMatrix4x4()
    :MyMatrix (4)
{
}

void MyMatrix4x4::rotate(float angle, float x, float y, float z)
{
    rotationQuaternions(angle, {x,y,z});
}

void MyMatrix4x4::translate(float x, float y, float z)
{
    translation({x,y,z});
}

void MyMatrix4x4::scale(float x, float y, float z)
{
    scaling({x,y,z});
}

QMatrix4x4 MyMatrix4x4::matrix()
{
    return QMatrix4x4(MyMatrix::matrix());
}

QMatrix4x4 MyMatrix4x4::matrixAtPoint(QVector3D p)
{
    translationAtPoint({p.x(),p.y(),p.z()});
    QMatrix4x4 r = matrix();
    translationAtPoint({-p.x(),-p.y(),-p.z()});
    return r;
}

void MyMatrix4x4::perspective(float verticalAngle, float aspectRatio, float nearPlane, float farPlane)
{
    float scale = tan(verticalAngle * 0.5f * M_PI / 180) * nearPlane;
    float r = aspectRatio * scale;
    float l= -r;
    float t = scale;
    float b = -t;
    frustum(l,r,b,t,nearPlane,farPlane);
}

void MyMatrix4x4::frustum(float left, float right, float bottom, float top, float nearPlane, float farPlane)
{
    float mat_s[16] = {
        2 * nearPlane / (right - left),
        0,
        0,
        0,

        0,
        2 * nearPlane / (top - bottom),
        0,
        0,

        (right + left) / (right - left),
        (top + bottom) / (top - bottom),
        -(farPlane + nearPlane) / (farPlane - nearPlane),
        -1,

        0,
        0,
        -2 * farPlane * nearPlane / (farPlane - nearPlane),
        0
    };

    productMatrix(mat_s);
}
