// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <QVector3D>

static const QString vertexShaderFile   = ":/shaders/vertex.glsl";
static const QString fragmentShaderFile = ":/shaders/fragment.glsl";

static const unsigned NB_TRI_SQUARE = 2;
static const unsigned NB_TRI_CIRCLE = 15;

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);
    setFormat(sf);

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    timer = new QTimer(this);
    timer->setInterval(50);  // msec
    connect (timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer->start();
    elapsedTimer.start();
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete timer;

    makeCurrent();
    tearGLObjects();
    doneCurrent();
}


void GLArea::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glEnable(GL_DEPTH_TEST);

    makeGLObjects();

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

}

void GLArea::reloadPoints(Bresenham b)
{
    b.calculBresenham();
    points.clear();
    points = b.points();

    float dist = std::max(abs(points[0].x()-points[points.size()-1].x()),
                          abs(points[0].y()-points[points.size()-1].y()));
    if (dist > 20)
        zPos = -(dist);

    if (nbDisplayed >= points.size()-1)
        nbDisplayed = points.size()-2;
}

void GLArea::changeFirstPoint(QVector2D p)
{
    Bresenham b(p, points.at(points.size()-1));
    reloadPoints(b);
}

void GLArea::changeLastPoint(QVector2D p)
{
    Bresenham b(points.at(0), p);
    reloadPoints(b);
}

void GLArea::resetPosition()
{
    xRot=0.0f, yRot=0.0f, zRot=0.0f;
    zPos=-20.0f;
    auto_rot = 0.0f;
    if (!in3D) auto_rotate = false;
}

QVector<GLfloat> GLArea::generateSquareVertices()
{
    nb_tri = NB_TRI_SQUARE;

    GLfloat vs[] = {        // vertices
        -0.0, -0.0, 0.0,    // 0 A
        -0.0,  1.0, 0.0,    // 1 B
         1.0, -0.0, 0.0,    // 2 C
         1.0,  1.0, 0.0,    // 3 D
    };

                   // A  C  B  A  B  D
    int ind_ver[] = { 0, 1, 2, 2, 1, 3 };

    QVector<GLfloat> vec_vs;

    for (int i = 0; i < nb_tri*3; ++i)
    {

        for (int j = 0; j < 3; j++)
        {
            vec_vs.append(vs[ind_ver[i]*3+j]);
        }
    }
    return vec_vs;
}

QVector<GLfloat> GLArea::generateCircleVertices()
{
    nb_tri = NB_TRI_CIRCLE;

    float r = 0.5f;
    GLfloat* vs = new GLfloat[(nb_tri+1)*3];
    vs[0] = vs[1] = 0.5f;
    vs[2] = 0.0f;
    for (unsigned i = 1 ; i < nb_tri+1 ; ++i)
    {
        vs[i*3]   = r * cos(2*M_PI * (i-1)/nb_tri)+0.5f ;
        vs[i*3+1] = r * sin(2*M_PI * (i-1)/nb_tri)+0.5f;
        vs[i*3+2] = 0.0f ;
    }

    int* ind_ver = new int[nb_tri*3];
    for (unsigned i = 0 ; i < nb_tri ; ++i)
    {
        ind_ver[i*3]   = 0;
        ind_ver[i*3+1] = i+1;
        ind_ver[i*3+2] = std::max(int((i+2)%(nb_tri+1)),1);
    }

    QVector<GLfloat> vec_vs;

    for (int i = 0; i < nb_tri*3; ++i)
    {

        for (int j = 0; j < 3; j++)
        {
            vec_vs.append(vs[ind_ver[i]*3+j]);
        }
    }

    delete[] ind_ver;
    delete[] vs;

    return vec_vs;
}

void GLArea::makeGLObjects()
{
    rulers = Graduations();

    Bresenham b(QVector2D(0.0,0.0), QVector2D(20,20));
    reloadPoints(b);

    QVector<GLfloat> vs_circle = generateCircleVertices();
    QVector<GLfloat> vs_square = generateSquareVertices();

    GLfloat colors[] = {
        1.0, 0.2, 0.2,      // triangle 0
        1.0, 1.0, 0.6,
        0.5, 1.0, 0.2,      // triangle 1
    };
    int ind_col1[] = { 0, 0, 0, 0, 0, 0 };
    int ind_col2[] = { 2, 2, 2, 2, 2, 2 };


    QVector<GLfloat> vertData;
    QVector<GLfloat> vertData_points;
    QVector<GLfloat> vertDataC;
    QVector<GLfloat> vertDataC_points;

    for (int i = 0; i < NB_TRI_SQUARE*3; ++i)
    {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
        {
            vertData.append(vs_square[i*3+j]);
            vertData_points.append(vs_square[i*3+j]);
        }
        // couleurs sommets
        for (int j = 0; j < 3; j++)
        {
            vertData.append(colors[ind_col1[i%6]*3+j]);
        }
        for (int j = 0; j < 3; j++)
            vertData_points.append(colors[ind_col2[i%6]*3+j]);
    }

    for (int i = 0; i < NB_TRI_CIRCLE*3; ++i)
    {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
        {
            vertDataC.append(vs_circle[i*3+j]);
            vertDataC_points.append(vs_circle[i*3+j]);
        }
        // couleurs sommets
        for (int j = 0; j < 3; j++)
            vertDataC.append(colors[ind_col1[i%6]*3+j]);
        for (int j = 0; j < 3; j++)
            vertDataC_points.append(colors[ind_col2[i%6]*3+j]);
    }



    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));

    m_vbo_points.create();
    m_vbo_points.bind();
    m_vbo_points.allocate(vertData_points.constData(), vertData_points.count() * sizeof(GLfloat));

    m_vbo_circles.create();
    m_vbo_circles.bind();
    m_vbo_circles.allocate(vertDataC.constData(), vertDataC.count() * sizeof(GLfloat));

    m_vbo_points_circles.create();
    m_vbo_points_circles.bind();
    m_vbo_points_circles.allocate(vertDataC_points.constData(), vertDataC_points.count() * sizeof(GLfloat));
}


void GLArea::tearGLObjects()
{
    m_vbo.destroy();
    m_vbo_points.destroy();
    m_vbo_circles.destroy();
    m_vbo_points_circles.destroy();
}


void GLArea::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    windowRatio = double(w) / h;
}

void GLArea::paintGL()
{
    //qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program

    // Matrice de vue (caméra)

    QVector3D center;
    center.setX(-(points[0].x()+points[points.size()-1].x())/2);
    center.setY(-(points[0].y()+points[points.size()-1].y())/2);
    center.setZ(0);

    MyMatrix4x4 viewM;
    {

        QVector2D p1 = points.at(0);
        viewM.translate(-p1);
        viewM.rotate(xRot, 1,0,0);
        viewM.rotate(yRot, 0,1,0);
        viewM.rotate(auto_angle*auto_rot, 0,1,0);
//        viewM.rotate(xRot, 0,0,1);
        viewM.translate(p1);
        viewM.translate(xPos,yPos,zPos);
        viewM.translate(center);
        viewM.perspective(40.0f, windowRatio, 1.0f, 5.0f);
    }

    {
        QMatrix4x4 modelMatrix = viewM.matrixAtPoint({0,0,0});
        m_program->setUniformValue("modelMatrix", modelMatrix);

        rulers.display(m_program);
    }



    for (unsigned i = 0 ; i < points.size() ; i+= points.size()-1)
    {
        QVector2D p = points[i];

        if (mode == MODE_CARRE)  m_vbo.bind();
        if (mode == MODE_CERCLE) m_vbo_circles.bind();

        m_program->enableAttributeArray("in_position");
        m_program->enableAttributeArray("in_uv");
        m_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        m_program->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6*sizeof(GLfloat));
//m_program->setAttributeBuffer(location, enum, offset, typle, stride)

        QMatrix4x4 modelMatrix = viewM.matrixAtPoint(p);

        m_program->setUniformValue("modelMatrix", modelMatrix);

        glDrawArrays(GL_TRIANGLES, 0, nb_tri*3);

        m_program->disableAttributeArray("in_position");
        m_program->disableAttributeArray("in_uv");

        if (mode == MODE_CARRE)  m_vbo.release();
        if (mode == MODE_CERCLE) m_vbo_circles.release();
    }


    unsigned i = 0;
    for (auto it = points.begin()+1 ; it != points.end() && i < nbDisplayed ; ++it )
    {
        QVector2D p = *it;

        if (mode == MODE_CARRE)  m_vbo_points.bind();
        if (mode == MODE_CERCLE) m_vbo_points_circles.bind();

        m_program->enableAttributeArray("in_position");
        m_program->enableAttributeArray("in_uv");
        m_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        m_program->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6*sizeof(GLfloat));
//m_program->setAttributeBuffer(location, enum, offset, typle, stride)

        QMatrix4x4 modelMatrix = viewM.matrixAtPoint(p);

        m_program->setUniformValue("modelMatrix", modelMatrix);

        glDrawArrays(GL_TRIANGLES, 0, nb_tri*3);

        m_program->disableAttributeArray("in_position");
        m_program->disableAttributeArray("in_uv");

        if (mode == MODE_CARRE)  m_vbo_points.release();
        if (mode == MODE_CERCLE) m_vbo_points_circles.release();

        ++i;
    }

    m_program->release();
}

void GLArea::increase()
{
    nbDisplayed = std::min(nbDisplayed+1, int(points.size()-2));
    update();
}

void GLArea::decrease()
{
    nbDisplayed = std::max(nbDisplayed-1, 0);
    update();
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key())
    {
        case Qt::Key_I:
            xRot=20.0f; yRot=0.0f; zRot=0.0f;
            xPos=0.0f;  yPos=0.0f; zPos=-50.0f;
            update();
            break;
        case Qt::Key_Plus:
            increase();
            break;
        case Qt::Key_Minus:
            decrease();
            break;
        case Qt::Key_Space:
            if (in3D)
                auto_rotate = !auto_rotate;
            update();
            break;
        default:
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    lastPos = ev->pos();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    int dx = ev->x() - lastPos.x();
    int dy = ev->y() - lastPos.y();

    if (ev->buttons() & Qt::LeftButton) {
        if (in3D)
        {
            xRot += dy/10.0f;
            yRot += dx/10.0f;
            update();
        }
    } else if (ev->buttons() & Qt::RightButton) {
        xPos += dx/10.0f;
        yPos -= dy/10.0f;
        update();
    } else if (ev->buttons() & Qt::MidButton) {
        if (in3D)
        {
            xPos += dx/10.0f;
            zPos += dy/5.0f;
            update();
        }
    }

    lastPos = ev->pos();
}

void GLArea::onTimeout()
{
    static qint64 old_chrono = elapsedTimer.elapsed(); // static : initialisation la première fois et conserve la dernière valeur
    qint64 chrono = elapsedTimer.elapsed();
    dt = (chrono - old_chrono) / 1000.0f;
    if (auto_rotate)
    {
       auto_rot = std::min(auto_rot+0.01f, 1.0f);
//        xRot += (0.01f) * ((int(chrono/1000)%18 > 9)*2-1);
//        qDebug() << xRot << chrono/1000;
    }
    old_chrono = chrono;

    update();
}

void GLArea::changeAngleObj(double a)
{
    auto_angle = float(a);
    auto_rot = 0.0f;
    auto_rotate = false;
}

void GLArea::tourner()
{
    if (in3D)
    {
        auto_rot = 0.0f;
        auto_rotate = true;
    }
}

void GLArea::changerMode(int m)
{
    mode = static_cast<modes>(m);

    qDebug() << mode;

    if (mode == MODE_CARRE) nb_tri = NB_TRI_SQUARE;
    if (mode == MODE_CERCLE) nb_tri = NB_TRI_CIRCLE;

    qDebug() << nb_tri;
}

