#ifndef GRID_H
#define GRID_H

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QVector>
#include <QVector3D>

class Grid
{
private:
    QOpenGLBuffer vbo_;
    QVector3D color_;
    unsigned nb_line;

    static unsigned height_;
    static unsigned rate_;
public:
    Grid(QVector3D color = {.7f,.7f,.7f});
    void display(QOpenGLShaderProgram* sp);
};

#endif // GRID_H
