#include "bresenham.h"

Bresenham::Bresenham(QVector2D depart, QVector2D arrive)
    :depart_(depart), arrive_(arrive)
{
    delta_x = arrive_.x() - depart_.x();
    delta_y = arrive_.y() - depart_.y();

    swaping_xy();
    swaping_nxy();
}

QVector2D Bresenham::editer(int x, int y)
{
    QVector2D point;
    if (swap_xy)
        point = depart_+QVector2D(y,x);
    else
        point = depart_+QVector2D(x,y);
    return point;
}


void Bresenham::swaping_xy()
{
    if (abs(delta_y) > abs(delta_x))
    {
        swap_xy = true;
        int temp = delta_x;
        delta_x = delta_y;
        delta_y = temp;
    }
    else
        swap_xy = false;
}

void Bresenham::swaping_nxy()
{
    if (delta_x < 0)
    {
        swap_xn = true;
        delta_x = -delta_x;
    }
    else
        swap_xn = false;

    if (delta_y < 0)
    {
        swap_yn = true;
        delta_y = -delta_y;
    }
    else
        swap_yn = false;
}

void Bresenham::calculBresenham()
{
    points_.clear();

    int x = 0;
    int y = 0;
    int d = 2*delta_y-delta_x;
    int i1 = 2*delta_y;
    int i2 = 2*delta_y-2*delta_x;

    points_.push_back(editer(x,y));

    while(abs(x) != delta_x && points_.size() < 1000)
    {
        if (!swap_xn)
            x++;
        else
            x--;

        if (d < 0)
            d+=i1;
        else
        {
            d+=i2;
            if (!swap_yn)
                y++;
            else
                y--;
        }
        points_.push_back(editer(x,y));
    }
}

void Bresenham::afficher()
{
    if (points_.empty())
        calculBresenham();
    for (auto& p : points_)
        qDebug() << p;
}
