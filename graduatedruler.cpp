#include "graduatedruler.h"

unsigned GraduatedRuler::height_ = 50;
unsigned GraduatedRuler::rate_   = 1;
float GraduatedRuler::grad_size_ = 0.5f;

GraduatedRuler::GraduatedRuler(GradType t, QVector3D color, bool in3D)
    :color_(color)
{
    int coor = (static_cast<int>(t)-1)%3;

    nb_line = height_*2/rate_ +2;
    GLfloat* vertices = new GLfloat[nb_line*2*3];

    vertices[0] = 0.; vertices[1] = -int(height_); vertices[2] = 0.02f;
    vertices[3] = 0.; vertices[4] =  height_; vertices[5] = 0.02f;

    unsigned grad = 6;
    for (int i = -int(height_) ; i <= int(height_) ; i+=rate_)
    {
        vertices[grad]   = 0;
        vertices[grad+1] = i+ 0.02f*(coor==1);
        vertices[grad+2] = 0.02f * (coor==0);

        if ((grad/6-1)%5 == 0)
            vertices[grad+3] = grad_size_*2*in3D;
        else
            vertices[grad+3] = grad_size_*in3D;

        vertices[grad+4] = i+ 0.02f*(coor==1);

        if ((grad/6-1)%5 == 0)
            vertices[grad+5] = grad_size_*2*!in3D + 0.02f * (coor==0);
        else
            vertices[grad+5] = grad_size_*!in3D + 0.02f * (coor==0);

        grad += 6;
    }

    QVector<GLfloat> vertData;
    for (int i = 0; i < nb_line*2; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData.append(vertices[i*3+(j+coor)%3]);
        // coordonnées texture
        for (int j = 0; j < 3; j++)
            vertData.append(color[j]);
    }

    vbo_.create();
    vbo_.bind();
    vbo_.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
}

void GraduatedRuler::display(QOpenGLShaderProgram *sp)
{
    vbo_.bind();

    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");
    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6*sizeof(GLfloat));

    glDrawArrays(GL_LINES, 0, nb_line*3);

    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");

    vbo_.release();
}
