#include "graduations.h"

Graduations::Graduations()
{
    rulers.push_back(GraduatedRuler(GraduatedRuler::X,{0.,1.,0.}));
    rulers.push_back(GraduatedRuler(GraduatedRuler::Y,{1.,0.,0.}));
    rulers.push_back(GraduatedRuler(GraduatedRuler::Z,{0.,0.,1.}, display3D));

    grid = Grid({.5f,.5f,.5f});
}

void Graduations::display(QOpenGLShaderProgram *sp)
{
    if (display3D)
        rulers.at(0).display(sp);
    rulers.at(1).display(sp);
    rulers.at(2).display(sp);

    grid.display(sp);
}

void Graduations::changeDisplay()
{
    display3D = !display3D;
    rulers.pop_back();
    rulers.push_back(GraduatedRuler(GraduatedRuler::Z,{0.,0.,1.}, display3D));
}
