#ifndef GRADUATEDRULER_H
#define GRADUATEDRULER_H

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QVector>
#include <QVector3D>

class GraduatedRuler
{
private:
    QOpenGLBuffer vbo_;
    QVector3D color_;
    unsigned nb_line;

    static unsigned height_;
    static unsigned rate_;
    static float grad_size_;

public:
    enum GradType {X,Y,Z};
    GraduatedRuler(GradType, QVector3D color, bool in3D = true);
    void display(QOpenGLShaderProgram* sp);

};

#endif // GRADUATEDRULER_H
