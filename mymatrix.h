#ifndef MYMATRIX_H
#define MYMATRIX_H

#include <QVector>
#include <QVector2D>
#include <QVector3D>

#include <QDebug>
#include <math.h>

class MyMatrix
{
private:
    float* matrix_;
    unsigned dimension_;

protected:
    float* matrix();

public:
    MyMatrix(unsigned dimension = 3);
    ~MyMatrix();

    float* I();

    static float* productMatrix(float* matrix1, float* matrix2, unsigned dim);
    void productMatrix(float* matrix2);
    void rproductMatrix(float* matrix2);

    void translation(QVector<float> t);
    void rotation   (float angle, QVector<float> rot);
    void scaling    (QVector<float> s);

    void rotationQuaternions (float angle, QVector3D rot);

    void translationAtPoint(QVector<float> t);

    QVector<float> cHomogenes(QVector<float> c);
    QVector<float> productPointMatrix(QVector<float> point);
    QVector<float> changePosition(QVector<float> position);

};

#endif // MYMATRIX_H
