#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T12:27:11
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

LIBS += -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FIG
TEMPLATE = app


SOURCES += main.cpp\
        princ.cpp \
        glarea.cpp \
    bresenham.cpp \
    mymatrix.cpp \
    mymatrix4x4.cpp \
    graduatedruler.cpp \
    graduations.cpp \
    grid.cpp

HEADERS  += princ.h \
        glarea.h \
    bresenham.h \
    mymatrix.h \
    mymatrix4x4.h \
    graduatedruler.h \
    graduations.h \
    grid.h

FORMS    += princ.ui

RESOURCES += \
    FIG.qrc
