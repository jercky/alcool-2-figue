// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef PRINC_H
#define PRINC_H

#include "ui_princ.h"

class Princ : public QMainWindow
{
    Q_OBJECT

public:
    explicit Princ(QWidget *parent = 0);
    Ui::Princ *ui;
};

#endif // PRINC_H
