#ifndef BRESENHAM_H
#define BRESENHAM_H

#include <QDebug>
#include <QVector2D>
#include <vector>

class Bresenham
{
private:
    QVector2D depart_;
    QVector2D arrive_;

    int delta_x;
    int delta_y;

    std::vector<QVector2D> points_;

    bool swap_xy;
    bool swap_xn;
    bool swap_yn;
public:
    Bresenham(QVector2D depart, QVector2D arrive);

    void calculBresenham();
    QVector2D editer (int x, int y);
    void afficher();

    void swaping_xy();
    void swaping_nxy();

    std::vector<QVector2D> points() const { return points_; }
};

#endif // BRESENHAM_H
