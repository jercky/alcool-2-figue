#include "mymatrix.h"
#include <cassert>

MyMatrix::MyMatrix(unsigned dimension)
    :dimension_(dimension)
{
    matrix_ = new float[dimension_*dimension_];
    for (unsigned j = 0 ; j < dimension_ ; ++j)
    {
        for (unsigned i = 0 ; i < dimension_ ; ++i)
        {
            if (i==j)
                matrix_[j*dimension_+i] = 1;
            else
                matrix_[j*dimension_+i] = 0;
        }
    }
}

MyMatrix::~MyMatrix()
{
    delete[] matrix_;
}

float* MyMatrix::I()
{
    float* identity = new float[dimension_*dimension_];

    for (unsigned j = 0 ; j < dimension_ ; ++j)
    {
        for (unsigned i = 0 ; i < dimension_ ; ++i)
        {
            if (i==j)
                identity[j*dimension_+i] = 1;
            else
                identity[j*dimension_+i] = 0;
        }
    }

    return identity;
}

float* MyMatrix::productMatrix(float *matrix1, float *matrix2, unsigned dim)
{
    float* product = new float[dim*dim];
    for (unsigned i = 0 ; i < dim*dim ; ++i) product[i] = 0;

    for (unsigned j = 0 ; j < dim ; ++j)
    {
        for (unsigned i = 0 ; i < dim ; ++i)
        {
            for (unsigned k = 0 ; k < dim ; ++k)
                product[i+j*dim] += matrix2[k+j*dim] * matrix1[i+k*dim];
        }
    }
    return product;
}

void MyMatrix::productMatrix(float *matrix2)
{
    float* product = productMatrix(matrix_,matrix2,dimension_);

    for (unsigned i = 0 ; i < dimension_*dimension_ ; ++i)
        matrix_[i] = product[i];

    delete[] product;
}

void MyMatrix::rproductMatrix(float *matrix2)
{
    float* product = productMatrix(matrix2,matrix_,dimension_);

    for (unsigned i = 0 ; i < dimension_*dimension_ ; ++i)
        matrix_[i] = product[i];

    delete[] product;
}

void MyMatrix::translation(QVector<float> t)
{
    assert(unsigned(t.size()+1) == dimension_);

    float* mat_t = new float[dimension_*dimension_];
    mat_t = I();

    for (unsigned i = 0 ; i < dimension_-1 ; ++i)
        mat_t[dimension_-1+dimension_*i] = t[i];

    productMatrix(mat_t);

    delete[] mat_t;
}

void MyMatrix::rotation(float angle, QVector<float> r)
{
    assert(unsigned(r.size()+1) == dimension_);

    float* mat_r = new float[dimension_*dimension_];
    mat_r = I();

    float a = cos(angle);
    float b = sin(angle);
    QVector<float> toinsert;
    toinsert.push_back(a);
    toinsert.push_back(-b);
    toinsert.push_back(b);
    toinsert.push_back(a);

    for (unsigned axes = 0 ; axes < dimension_-1; ++axes)
    {
        int count = 0;
        for (unsigned j = 0 ; j < dimension_-1 && count < 4 ; ++j)
        {
            for (unsigned i = 0 ; i < dimension_-1 && count < 4 ; ++i)
            {
                if ((axes == i || axes == j) && r[axes] > 0.0f)
                {
                    mat_r[j*(dimension_-1)+i] = toinsert[count]*r[axes];
                    count++;
                }

            }
        }
    }

    productMatrix(mat_r);

    delete[] mat_r;
}

void MyMatrix::rotationQuaternions(float angle, QVector3D rot)
{
    assert(dimension_ <= 4);

    rot.normalize();
    angle *= M_PI/(180.0f*2);

    float a = cosf(angle);
    float b = sinf(angle) * rot[0];
    float c = sinf(angle) * rot[1];
    float d = sinf(angle) * rot[2];

    float* mat_r = new float[dimension_*dimension_];
    mat_r = I();

    QVector<float> to_insert;
    to_insert.push_back(a*a+b*b-c*c-d*d);
    to_insert.push_back(2*b*c - 2*a*d);
    to_insert.push_back(2*a*c + 2*b*d);

    to_insert.push_back(2*a*d + 2*b*c);
    to_insert.push_back(a*a-b*b+c*c-d*d);
    to_insert.push_back(2*c*d - 2*a*b);

    to_insert.push_back(2*b*d - 2*a*c);
    to_insert.push_back(2*a*b + 2*c*d);
    to_insert.push_back(a*a-b*b-c*c+d*d);

    for (unsigned j = 0 ; j < 3 ; ++j)
        for (unsigned i = 0 ; i < 3 ; ++i)
            mat_r[j*dimension_+i] = to_insert.at(j*3+i);

    productMatrix(mat_r);

    delete[] mat_r;
}

void MyMatrix::scaling(QVector<float> s)
{
    assert(unsigned(s.size()+1) == dimension_);

    float* mat_s = new float[dimension_*dimension_];
    mat_s = I();

    for (unsigned i = 0 ; i < dimension_ ; ++i)
        mat_s[i*(dimension_+1)] = s[i];

    productMatrix(mat_s);

    delete[] mat_s;
}

QVector<float> MyMatrix::cHomogenes(QVector<float> point)
{
    QVector<float> r;
    for (auto& p : point)
        r.push_back(p);
    r.push_back(1);
    return r;
}

QVector<float> MyMatrix::productPointMatrix(QVector<float> point)
{
    QVector<float> product;
    for (unsigned i = 0 ; i < dimension_; ++i) product.push_back(0);

    for (unsigned j = 0 ; j < dimension_ ; ++j)
    {
        for (unsigned i = 0 ; i < dimension_ ; ++i)
        {
            product[j] += point[i] * matrix_[j*dimension_+i];
        }
    }

    return product;
}

QVector<float> MyMatrix::changePosition(QVector<float> position)
{
    QVector<float> point3D = cHomogenes(position);
    point3D = productPointMatrix(point3D);

    QVector<float> r;
    for (unsigned i = 0 ; i < dimension_ ; ++i)
        r.push_back(point3D[i]/point3D[dimension_-1]);

    return r;
}

void MyMatrix::translationAtPoint(QVector<float> t)
{
    assert(unsigned(t.size()+1) == dimension_);

    float* mat_t = new float[dimension_*dimension_];
    mat_t = I();

    for (unsigned i = 0 ; i < dimension_-1 ; ++i)
        mat_t[dimension_-1+dimension_*i] = t[i];

    rproductMatrix(mat_t);

    delete[] mat_t;
}

float* MyMatrix::matrix()
{
    return matrix_;
}
